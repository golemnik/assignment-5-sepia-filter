#!/bin/bash --

#!/bin/bash --

TRANSFORMER=$1
FILE=$2
ITERATIONS=$3

S1=0
S2=0

R1=""
R2=""

k=$ITERATIONS
while [ $k != 0 ]; do
  T1=$( TIMEFORMAT="%U"; { time ${TRANSFORMER} $FILE /dev/null sepia-asm; } 2>&1 )
  T2=$( TIMEFORMAT="%U"; { time ${TRANSFORMER} $FILE /dev/null sepia-c; } 2>&1 )

  R1="$R1 $T1"
  R2="$R2 $T2"

  T1="10#"${T1/./}
  T2="10#"${T2/./}

  S1=$(( $S1 + $T1 ))
  S2=$(( $S2 + $T2 ))
  k=$(( $k - 1 ))
done

AVG1=$(( $S1 / $ITERATIONS ))
AVG2=$(( $S2 / $ITERATIONS ))

if [ $(( $AVG1 < $AVG2 )) == 1 ]; then
  CONCLUSION="FASTER"
else
  CONCLUSION="not faster"
fi


echo "ASM average time (msec): $AVG1, C average time (msec): $AVG2, conclusion: $CONCLUSION"
echo "ASM measurements (sec): $R1"
echo "C measurements (sec)  : $R2"
