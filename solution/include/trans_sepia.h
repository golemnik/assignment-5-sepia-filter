#ifndef __TRANS_SEPIA_H__
#define __TRANS_SEPIA_H__

#include "image.h"
#include "transformer.h"

enum transform_status sepia_c( const struct image* source, struct image* target );
enum transform_status sepia_asm( const struct image* source, struct image* target );

#endif
