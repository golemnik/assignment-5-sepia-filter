#include "bmp_header.h"
#include "file_reader.h"
#include "image_allocator.h"

#include <errno.h>
#include <string.h>

static const uint16_t EXPECTED_HDR_TYPE = 0x4D42; // BM
static const uint32_t EXPECTED_HDR_SIZE = 40;
static const uint16_t EXPECTED_HDR_PLANES = 1;
static const uint16_t EXPECTED_HDR_BIT_COUNT = 24;
static const uint32_t EXPECTED_HDR_COMPRESSION = 0;

#define ALIGNMENT 4

static size_t calc_row_size(uint32_t width) {
    uint64_t row = width * BYTES_PER_PIXEL;
//    return (0 == row % ALIGNMENT) ? row : (row / ALIGNMENT + 1) * ALIGNMENT;
    return (row + 3) & 0xFFFFFFFFFFFFFFFC;
}

enum read_status from_bmp( FILE* in, struct image* img )
{
    struct bmp_header hdr;

    size_t rc = fread(&hdr, sizeof(hdr), 1, in);
    if (1 != rc) {
        fprintf(stderr, "ERRNO: %d %s\n", errno, strerror(errno));
        return READ_INVALID_HEADER; // TODO: can be related to errno
    }
    if (EXPECTED_HDR_TYPE != hdr.bfType) {
        return READ_INVALID_SIGNATURE;
    }
    if (EXPECTED_HDR_BIT_COUNT != hdr.biBitCount) {
        return READ_INVALID_BITS;
    }
    if (EXPECTED_HDR_SIZE != hdr.biSize
            || EXPECTED_HDR_PLANES != hdr.biPlanes
            || EXPECTED_HDR_COMPRESSION != hdr.biCompression
            || hdr.bOffBits < sizeof(hdr)
            || 0 == hdr.biWidth
            || 0 == hdr.biHeight) {
        fprintf(stderr, "from_bmp: sz=%d w=%d h=%d\n", hdr.biSize, hdr.biWidth, hdr.biHeight);
        return READ_INVALID_HEADER; // TODO: can be related to an unexpected attr
    }

    int sc = fseek(in, hdr.bOffBits, SEEK_SET);
    if (0 != sc) {
        fprintf(stderr, "ERRNO: %d %s\n", errno, strerror(errno));
        return READ_INVALID_DATA_OFFSET; // TODO: can be related to errno
    }

    img->width = hdr.biWidth;
    img->height = hdr.biHeight;
    img->x_resolution = hdr.biXPelsPerMeter;
    img->y_resolution = hdr.biYPelsPerMeter;

    size_t row_size = calc_row_size(img->width);
    void* row;
    enum alloc_status as = alloc_row(row_size, &row);
    if (ALLOC_OK != as) {
        return READ_NO_MEM;
    }

    enum read_status rs = READ_OK;
    as = alloc_image(img);
    if (ALLOC_OK != as) {
        rs = READ_NO_MEM;
    } else {
      struct pixel* data = img->data;
      for (uint64_t r = 0; r < img->height && READ_OK == rs; r++) {
        size_t rc_read = fread(row, row_size, 1, in);
        if (1 != rc_read) {
            fprintf(stderr, "ERRNO a=%ld e=%lu/%u: %d %s\n", rc_read, img->width, hdr.biWidth, errno, strerror(errno));
            rs = READ_ERROR; // TODO: can be related to errno
            break;
        }
        uint8_t* buf = row;
        for (uint64_t c = 0; c < img->width; c++) {
            data->b = *buf++;
            data->g = *buf++;
            data->r = *buf++;
            data++;
        }
      }
    }

    enum free_status fs_row = free_row(row);
    if (FREE_OK != fs_row) {
        fprintf(stderr, "Unable to free row buffer, error=%d\n", fs_row);
    }

    if (READ_OK != rs) {
        enum free_status fs = free_image(img);
        if (FREE_OK != fs) {
            fprintf(stderr, "Unable to free image buffer, error=%d\n", fs);
        }
    }

    return rs;
}


enum write_status to_bmp( FILE* out, struct image const* img )
{
    if (img->width > UINT32_MAX || img->height > UINT32_MAX ) {
        fprintf(stderr, "to_bmp: w=%ld h=%ld\n", img->width, img->height);
        return WRITE_INVALID_DIMENSION;
    }
    size_t row_size = calc_row_size(img->width);
    uint64_t file_size = sizeof(struct bmp_header) + row_size * img->height;
    if (file_size > UINT32_MAX) {
        fprintf(stderr, "to_bmp: file size=%ld\n", file_size);
        return WRITE_TOO_LONG_FILE;
    }

    struct bmp_header hdr;
    hdr.bfType = EXPECTED_HDR_TYPE;
    hdr.bfileSize = file_size;
    hdr.bfReserved = 0;
    hdr.bOffBits = sizeof(hdr);
    hdr.biSize = EXPECTED_HDR_SIZE;
    hdr.biWidth = img->width;
    hdr.biHeight = img->height;
    hdr.biPlanes = EXPECTED_HDR_PLANES;
    hdr.biBitCount = EXPECTED_HDR_BIT_COUNT;
    hdr.biCompression = EXPECTED_HDR_COMPRESSION;
    hdr.biSizeImage = 0;
    hdr.biXPelsPerMeter = img->x_resolution;
    hdr.biYPelsPerMeter = img->y_resolution;
    hdr.biClrUsed = 0;
    hdr.biClrImportant = 0;

    size_t rc = fwrite(&hdr, sizeof(hdr), 1, out);
    if (1 != rc) {
        fprintf(stderr, "ERRNO: %d %s\n", errno, strerror(errno));
        return WRITE_ERROR; // TODO: can be related to errno
    }

    void* row;
    enum alloc_status as = alloc_row(row_size, &row);
    if (ALLOC_OK != as) {
        return WRITE_NO_MEM;
    }

    enum write_status ws = WRITE_OK;
    struct pixel* data = img->data;
    for (uint64_t r = 0; r < img->height && WRITE_OK == ws; r++) {
        uint8_t* buf = row;
        uint64_t c = 0;
        for (; c < img->width; c++) {
            *buf++ = data->b;
            *buf++ = data->g;
            *buf++ = data->r;
            data++;
        }
        for (c = c*3; c < row_size; c++) {
            *buf++ = 0;
        }
        size_t rc_write = fwrite(row, row_size, 1, out);
        if (1 != rc_write) {
            fprintf(stderr, "ERRNO: %d %s\n", errno, strerror(errno));
            ws = WRITE_ERROR; // TODO: can be related to errno
            break;
        }
    }

    enum free_status fs = free_row(row);
    if (FREE_OK != fs) {
        fprintf(stderr, "Unable to free row buffer, error=%d\n", fs);
    }

    return ws;
}
