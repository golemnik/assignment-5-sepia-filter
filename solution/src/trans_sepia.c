#include "image.h"
#include "image_allocator.h"
#include "trans_sepia.h"
#include "trans_util.h"
#include "transformer.h"

#include <memory.h>

void do_sepia_asm(const struct pixel* source, struct pixel* target, uint64_t size);

static inline uint8_t normalize(const float val) {
    uint32_t ret = (uint32_t)(val + .5f);
    return ret > 255 ? 255 : ret;
}

static void do_sepia_c(const struct pixel* source, struct pixel* target, const uint64_t size) {
    for (uint64_t r = 0; r < size; r++) {
        target->r = normalize( ((float)source->r * .393f) + ((float)source->g * .769f) + ((float)source->b * .189f) );
        target->g = normalize( ((float)source->r * .349f) + ((float)source->g * .686f) + ((float)source->b * .168f) );
        target->b = normalize( ((float)source->r * .272f) + ((float)source->g * .534f) + ((float)source->b * .131f) );

        target++;
        source++;
    }
}


enum mode { WITH_C, WITH_ASM };

static enum transform_status sepia( const struct image* source, struct image* target, enum mode mode )
{
    header_copy(source, target);
    enum alloc_status as = alloc_image(target);
    memset(target->data, '\0', source->height * source->width * sizeof(struct pixel));
    if (ALLOC_OK != as) {
        return TRANSFORM_NO_MEM;
    }
    switch (mode) {
        case WITH_ASM:
            do_sepia_asm(source->data, target->data, source->height * source->width);
            break;
        case WITH_C:
            do_sepia_c(source->data, target->data, source->height * source->width);
            break;
        default:
            return TRANSFORM_ERROR;
    }
    return TRANSFORM_OK;
}

enum transform_status sepia_c( const struct image* source, struct image* target) {
    return sepia(source, target, WITH_C);
}

enum transform_status sepia_asm( const struct image* source, struct image* target) {
    return sepia(source, target, WITH_ASM);
}
