#include "image_allocator.h"
#include "trans_rotate.h"
#include "trans_util.h"

#include <string.h>

enum transform_status rotate_0( const struct image* source, struct image* target )
{
    header_copy(source, target);
    enum alloc_status as = alloc_image(target);
    if (ALLOC_OK != as) {
        return TRANSFORM_NO_MEM;
    }
    memcpy(target->data, source->data, target->width*target->height*sizeof(struct pixel));

    return TRANSFORM_OK;
}

static uint64_t mirror(uint64_t pos, uint64_t width) {
    return width - pos - 1;
}

static uint64_t to_index(uint64_t row, uint64_t col, uint64_t width) {
    return row * width + col;
}

enum transform_status rotate_270( const struct image* source, struct image* target )
{
    header_rotate(source, target);
    enum alloc_status as = alloc_image(target);
    if (ALLOC_OK != as) {
        return TRANSFORM_NO_MEM;
    }
    struct pixel* src_pixel = source->data;
    for (uint64_t r = 0; r < source->height; r++) {
        for (uint64_t c = 0; c < source->width; c++) {
            uint64_t target_r = c;
            uint64_t target_c = mirror(r, target->width);
            uint64_t target_idx = to_index(target_r, target_c, target->width);
            memcpy(&(target->data[target_idx]), src_pixel++, sizeof(struct pixel));
        }
    }

    return TRANSFORM_OK;
}

enum transform_status rotate_180( const struct image* source, struct image* target )
{
    header_copy(source, target);
    enum alloc_status as = alloc_image(target);
    if (ALLOC_OK != as) {
        return TRANSFORM_NO_MEM;
    }
    struct pixel* src_pixel = source->data;
    for (uint64_t r = 0; r < source->height; r++) {
        for (uint64_t c = 0; c < source->width; c++) {
            uint64_t target_r = mirror(r, target->height);
            uint64_t target_c = mirror(c, target->width);
            uint64_t target_idx = to_index(target_r, target_c, target->width);
            memcpy(&(target->data[target_idx]), src_pixel++, sizeof(struct pixel));
        }
    }

    return TRANSFORM_OK;
}

enum transform_status rotate_90( const struct image* source, struct image* target )
{
    header_rotate(source, target);
    enum alloc_status as = alloc_image(target);
    if (ALLOC_OK != as) {
        return TRANSFORM_NO_MEM;
    }
    struct pixel* src_pixel = source->data;
    for (uint64_t r = 0; r < source->height; r++) {
        for (uint64_t c = 0; c < source->width; c++) {
            uint64_t target_r = mirror(c, target->height);
            uint64_t target_c = r;
            uint64_t target_idx = to_index(target_r, target_c, target->width);
            memcpy(&(target->data[target_idx]), src_pixel++, sizeof(struct pixel));
        }
    }

    return TRANSFORM_OK;
}
