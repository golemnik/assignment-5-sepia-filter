#include "file_opener.h"
#include "file_reader.h"
#include "image_allocator.h"
#include "transformer.h"

#include <stdio.h>

enum result_code {
    RC_OK = 0,
    RC_HELP,
    RC_ERROR_AT_READING,
    RC_ERROR_AT_TRANSFORMING,
    RC_ERROR_AT_WRITING
};

int save_transformed_image(const char *target_file_name,
                           struct image *target_image) {
    int rc;
    FILE* target_file;
    enum open_status os = open_file(target_file_name, &target_file, OPEN_FOR_WRITING);
    if (OPEN_OK != (os)) {
        fprintf(stderr, "Unable to open target file %s, error=%d\n", target_file_name, (os));
        rc = RC_ERROR_AT_WRITING;
    } else {
        enum write_status ws = to_bmp(target_file, target_image);
        if (WRITE_OK != ws) {
            fprintf(stderr, "Unable to write target file %s, error=%d\n", target_file_name, ws);
            rc = RC_ERROR_AT_WRITING;
        }
        enum close_status cs = close_file(target_file);
        if (CLOSE_OK != (cs)) {
            fprintf(stderr, "Unable to close target file %s, error=%d\n", target_file_name, (cs));
            rc = RC_ERROR_AT_WRITING;
        }
        if (WRITE_OK == ws && CLOSE_OK == (cs)) {
            rc = RC_OK;
        }
    }

    enum free_status fs = free_image(target_image);
    if (FREE_OK != (fs)) {
        fprintf(stderr, "Unable to free target image, error=%d\n", (fs));
    }
    return rc;
}

int transform_image(const char *transformation,
                    struct image *source_image,
                    struct image *target_image)
{
    int rc = RC_OK;
    enum transform_status ts = transform(source_image, target_image, transformation);
    if (TRANSFORM_OK != (ts)) {
        fprintf(stderr, "Unable to transform image, error=%d\n", (ts));
        (rc) = RC_ERROR_AT_TRANSFORMING;
    }
    enum free_status fs = free_image(source_image);
    if (FREE_OK != (fs)) {
        fprintf(stderr, "Unable to free source image, error=%d\n", (fs));
    }
    return rc;
}

int read_picture(const char *source_file_name, struct image *source_image) {
    FILE* source_file;
    enum read_status rs = 0;
    int rc = RC_OK;
    enum open_status os = open_file(source_file_name, &source_file, OPEN_FOR_READING);
    if (OPEN_OK != (os)) {
        fprintf(stderr, "Unable to open source file %s, error=%d\n", source_file_name, (os));
        (rc) = RC_ERROR_AT_READING;
    } else {
        (rs) = from_bmp((source_file), source_image);
        if (READ_OK != (rs)) {
           fprintf(stderr, "Unable to read source file %s, error=%d\n", source_file_name, (rs));
           (rc) = RC_ERROR_AT_READING;
        }
        enum close_status cs = close_file((source_file));
        if (CLOSE_OK != (cs)) {
            fprintf(stderr, "Unable to close source file %s, error=%d\n", source_file_name, (cs));
//            (rc) = RC_ERROR_AT_READING;
        }
    }
    return rc;
}

int main(int argc, char** argv )
{

    if (4 != argc) {
        fprintf(stderr,
            "\nUsage: %s <source-image> <transformed-image> <angle>"
            "\nWhere <angle> = 0, 90, -90, 180, -180, 270, -270"
            "\n", argv[0]
        );
        return RC_HELP;
    }
    const char* source_file_name = argv[1];
    const char* target_file_name = argv[2];
    const char* transformation = argv[3];

    struct image source_image;
    int rc = read_picture(source_file_name, &source_image);

    struct image target_image;
    if (RC_OK == rc) {
        rc = transform_image(transformation, &source_image, &target_image);
    }

    if (RC_OK == rc) {
        rc = save_transformed_image(target_file_name, &target_image);
    }


    return rc;
}
