#include "trans_rotate.h"
#include "trans_sepia.h"

#include <string.h>

enum transform_status transform( const struct image* source, struct image* target, const char* transformation )
{
    enum transform_status st;
    if (0 == strcmp("0", transformation) || 0 == strcmp("-0", transformation) || 0 == strcmp("360", transformation) || 0 == strcmp("-360", transformation)) {
        st = rotate_0(source, target);
    } else if (0 == strcmp("90",  transformation) || 0 == strcmp("-270", transformation)) {
        st = rotate_90(source, target);
    } else if (0 == strcmp("180", transformation) || 0 == strcmp("-180", transformation)) {
        st = rotate_180(source, target);
    } else if (0 == strcmp("270", transformation) || 0 == strcmp("-90",  transformation)) {
        st = rotate_270(source, target);
    } else if (0 == strcmp("sepia-c", transformation) ) {
        st = sepia_c(source, target);
    } else if (0 == strcmp("sepia-asm", transformation) ) {
        st = sepia_asm(source, target);
    } else {
        st = TRANSFORM_UNKNOWN;
    }
    return st;
}
