DEFAULT REL
; In 64-bit mode, NASM will by default generate absolute addresses.
; The REL keyword makes it produce RIP-relative addresses.


global do_sepia_asm

section .data
align 16
;             green  blue   red       zero
red:    dd    0.769, 0.189, 0.393,    0.0
green:  dd    0.686, 0.168, 0.349,    0.0
blue:   dd    0.534, 0.131, 0.272,    0.0

half:   dd    0.5

section .text

; %1 - mmx register with coefficients
; xmm3 - source R,B,G
; rax = al - result
%macro transform 1
    movaps xmm4, xmm3
    mulps xmm4, %1            ; xmm4 = 0, r, b, g
    haddps xmm4, xmm4         ; xmm4 = _, _, 0+r, b+g
    haddps xmm4, xmm4         ; xmm4 = _, _, _, r+b+g
    addss  xmm4, xmm5         ; xmm4 = _, _, _, (r+b+g) + 0.5
    cvttss2si rax, xmm4       ; rax = floor(r+b+g + 0.5)
    cmp rax, r8               ; > 255 ?
    jb %%done
    mov rax, r8
%%done:                       ; rax = al = 255 or int(r+b+g)
%endmacro


; struct pixel { uint8_t b, g, r; }
; void do_sepia_asm(const struct pixel* source, struct pixel* target, const uint64_t size)
; rdi = source, rsi = target, rdx = size
do_sepia_asm:
    movaps xmm0, [red]
    movaps xmm1, [green]
    movaps xmm2, [blue]
    movss  xmm5, [half]
    mov r8, 255
    xor rax, rax
    cvtsi2ss xmm4, rax     ; xmm4 = _, _, _, 0
    unpcklps xmm4, xmm4    ; xmm4 = _, _, 0, 0
.loop:
    mov cx, [rdi]          ; cx = g b, cl = b, ch = g
    inc rdi
    inc rdi
    mov al, ch             ; rax = al = g
    cvtsi2ss xmm3, rax     ; xmm3 = _, _, _, g
    mov al, [rdi]          ; rax = al = r
    inc rdi
    cvtsi2ss xmm4, rax     ; xmm4 = _, _, 0, r
    unpcklps xmm3, xmm4    ; xmm3 = 0, _, r, g
    mov al, cl
    cvtsi2ss xmm4, rax     ; xmm4 = _, _, 0, b
    unpcklps xmm3, xmm4    ; xmm3 = 0, r, b, g

    transform xmm1         ; rax = al = G
    mov cl, al             ; cl = G
    transform xmm2         ; rax = al = B
    mov ah, cl             ; ax = G B
    mov [rsi], ax
    inc rsi
    inc rsi
    transform xmm0         ; rax = al = R
    mov [rsi], al          ;
    inc rsi

    dec rdx
    jnz .loop
.done:
    ret
